//
//  File.swift
//  BibleGum
//
//  Created by Yin on 2018/4/19.
//  Copyright © 2018 Yin. All rights reserved.
//

import Foundation
import UIKit

class CalendarModel: NSObject, NSCoding {
    
    var id: Int = 0
    var image: UIImage? = nil
    var name: String = ""
    
    init(id: Int, name: String, image: UIImage) {
        self.id = id
        self.name = name
        self.image = image

    }

    required convenience init(coder aDecoder: NSCoder) {
        let id = aDecoder.decodeInteger(forKey: "id")
        let name = aDecoder.decodeObject(forKey: "name") as! String
        let image = aDecoder.decodeObject(forKey: "image") as! UIImage
        self.init(id: id, name: name, image: image)
    }

    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(image, forKey: "image")
    }
}



