//
//  Const.swift
//  BibleGum
//
//  Created by Yin on 2018/4/18.
//  Copyright © 2018 Yin. All rights reserved.
//

import Foundation
import UIKit

var coinArray = [String]()
var heartArray = [String]()
var calendarImages = [String]()
var calendarNames = [String]()

class Const {
    
    // Int
    static let CALENDAR_ITEM_COUNT  = 28
    static let FACE_IMAGE_COUNT     = 54
    static let OBJECT_IMAGE_COUNT   = 18
    
    // NSUserDefault Keys
    
    static let K_ARRAY_COINS        = "StringInCoins"
    static let K_ARRAY_HEART        = "StringInHeart"
    static let K_CALENDAR_ITEMS     = "CalendarItems"
    static let K_CALENDAR_IMAGES    = "CalendarImages"
    static let K_CALENDAR_NAMES     = "CalendarNames"
    
    
    // String
    static let LBL_A        = "Adoration: I praise and worship God, The Son, and The Holy Spirit."
    static let LBL_AA       = "He is infinite, the one and only, all-knowing, all-powerful, all-wise, unchanging, He is Love, everywhere, holy, pure, just, righteous, truthful, good, patient, and kind."
    static let LBL_C        = "Confession: I confess any sins that the Holy Spirit might reveal to me."
    static let LBL_CC       = "This is very important, for it keeps me humble before the cross!"
    static let LBL_T        = "Thanksgiving: I am thankful for all the many blessings God has provided for me."
    static let LBL_TT       = "Salvation from hell, family and friends, health, safety, etc. always so many things for which to be thankful."
    static let LBL_S        = "I surrender my life to you, Lord God, for everything I do, say and plan."
    static let LBL_SS       = "All my hopes, dreams and plans, every part of my life I hold out to You, for it is not mine but Yours, because You created me, died for me, saved me and love me! And You know what is best for me! I now live for You, Lord Jesus, not myself!"
    
    static let BUBLE_TITLE  = ["Helmet of Salvation",
                               "Breast Plate of Righteousness",
                               "Belt of Truth",
                               "Shod My Feet w/the Preparation of the Gospel ",
                               "Shield of Faith",
                               "Sword of the Spirit",]
    static let BUBLE_BODY   = ["As I put on the Helmet of Salvation, I can have confidence that the troubles of my world will not compare with the glories which I will see in Heaven.",
                               "When I have put on the Breast Plate of Righteousness, it will protect my heart from sinful and evil desires, for everything I do, flows out of it.",
                               "When I have fastened the Belt of Truth around me, it (God’s Word) will protect my mind in order to defeat Satan’s lies. His Truth shall be my standard.\n[I will take every thought captive to Jesus Christ; never emptying my mind, but instead thinking of whatever is True, Noble, Right, Lovely, Admirable, Excellent and Praiseworthy.]",                               
                               "I will tie on the footgear of the Gospel of Peace, so I can stand firm in the knowledge of what the Gospel means to me and others.  My footing will be sure and unshakable, as I prepare to carry the gospel to whomever God might send me.",
                               "I will pick up the shield of faith, to put out the fires from Satan’s flaming arrows, And I will have confidence that God is good and faithful and true to His Word, even if I have trouble in my life.",
                               "I will take hold of the Sword of the Spirit, which is the Word of God, every moment of my life.\nI will study God’s Word, learn from it and teach it, for it is effective to demolish Satan’s strongholds.",]
}
