//
//  DonationViewController.swift
//  BibleGum
//
//  Created by Yin on 2018/4/26.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

class DonationViewController: BaseViewController {

    @IBOutlet weak var btnDonate: UIButton!
    @IBOutlet weak var btnLater: UIButton!
    
    var activityIndicator = UIActivityIndicatorView()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        initView()
    }
    
    func initData() {
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        
    }
    
    func initView() {
        
        btnDonate.layer.borderWidth = 2
        btnDonate.layer.borderColor = UIColor.white.cgColor
        btnDonate.layer.cornerRadius = 5
        
        
        btnLater.layer.borderWidth = 2
        btnLater.layer.borderColor = UIColor.white.cgColor
        btnLater.layer.cornerRadius = 5
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.mkStoreKitProductPurchased,
                                               object: nil, queue: OperationQueue.main) { (note) -> Void in
                                                print ("Purchased product: \(String(describing: note.object))")
                                                self.activityIndicator.removeFromSuperview()
                                                print("success")
                                                self.perform(#selector(self.closeView), with: nil, afterDelay: 1.0)
                                               
                                                
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.mkStoreKitProductPurchaseFailed,
                                               object: nil, queue: OperationQueue.main) { (note) -> Void in
                                                print ("Purchased product: \(String(describing: note.object))")
                                                self.activityIndicator.removeFromSuperview()
                                                print("failed")
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.mkStoreKitDownloadCompleted,
                                               object: nil, queue: OperationQueue.main) { (note) -> Void in
                                                print ("Downloaded product: \(String(describing: note.userInfo))")
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.mkStoreKitDownloadProgress,
                                               object: nil, queue: OperationQueue.main) { (note) -> Void in
                                                print ("Downloading product: \(String(describing: note.userInfo))")
        }
        
        
    }
    
    @IBAction func closeAction(_ sender: Any) {
        closeView()
        
    }
    
    @objc func closeView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func agreeAction(_ sender: Any) {
        
        
        // Add it to the view where you want it to appear
        view.addSubview(activityIndicator)
        
        // Set up its size (the super view bounds usually)
        activityIndicator.frame = view.bounds
        // Start the loading animation
        activityIndicator.startAnimating()

        
        MKStoreKit.shared().initiatePaymentRequestForProduct(withIdentifier: "morning.mustard.donate")
    

    }


}
