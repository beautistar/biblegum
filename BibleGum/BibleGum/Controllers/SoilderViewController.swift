//
//  SoilderViewController.swift
//  BibleGum
//
//  Created by Yin on 2018/4/18.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit
import MZFormSheetPresentationController

class SoilderViewController: UIViewController {

    @IBOutlet weak var imvHeimet: UIImageView!
    @IBOutlet weak var imvWaist: UIImageView!
    @IBOutlet weak var imvLeg: UIImageView!
    @IBOutlet weak var imvChestSheild: UIImageView!
    @IBOutlet weak var imvSheild: UIImageView!
    @IBOutlet weak var imvSword: UIImageView!
    @IBOutlet weak var bubleView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    
    var selectedBodyIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        initView()
    }
    
    func initView() {
        
        imvSword.isHidden = true
        imvHeimet.isHidden = true
        imvLeg.isHidden = true
        imvChestSheild.isHidden = true
        imvSheild.isHidden = true
        imvSword.isHidden = true
        imvWaist.isHidden = true
        bubleView.isHidden = true
    }
    
    @IBAction func didTapped(_ sender: Any) {
        
        let btn = sender as! UIButton
        
        switch btn.tag {
        case 0:
            imvSword.isHidden = false
            
        case 1:
            imvWaist.isHidden = false
            
        case 2:
            imvSheild.isHidden = false
            
        case 3:
            imvChestSheild.isHidden = false
            
        case 4:
            imvHeimet.isHidden = false
            
        default:
            imvLeg.isHidden = false
            
        }
    }
    
    @IBAction func bodyPartTapped(_ sender: Any) {
        
        let bodyButton = sender as! UIButton
        
        switch bodyButton.tag {
        case 10:
            selectedBodyIndex = 0
        case 11:
            selectedBodyIndex = 1
        case 12:
            selectedBodyIndex = 2
        case 13:
            selectedBodyIndex = 3
        case 14:
            selectedBodyIndex = 4
        default:
            selectedBodyIndex = 5
        }
        
        bubleView.isHidden = false
        lblTitle.text = Const.BUBLE_TITLE[selectedBodyIndex]
        lblDetail.text = Const.BUBLE_BODY[selectedBodyIndex]
    }
    
    @IBAction func bubeTapped(_ sender: Any) {
        
        let viewController = self.storyboard!.instantiateViewController(withIdentifier: "BigBubleViewController") as! BigBubleViewController
        viewController.selectedIndex = selectedBodyIndex
        let formSheetController = MZFormSheetPresentationViewController(contentViewController: viewController)
        formSheetController.presentationController?.contentViewSize = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height-200)
        formSheetController.presentationController?.shouldDismissOnBackgroundViewTap = true
        formSheetController.contentViewControllerTransitionStyle = MZFormSheetPresentationTransitionStyle.bounce
        //formSheetController.presentationController?.isTransparentTouchEnabled = false
        self.present(formSheetController, animated: true, completion: nil)
    }
    
    @IBAction func backAction(_ sender: Any) {
    
        self.navigationController?.popViewController(animated: true)
    }

}
