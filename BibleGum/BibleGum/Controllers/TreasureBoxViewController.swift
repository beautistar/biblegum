//
//  TreasureBoxViewController.swift
//  BibleGum
//
//  Created by Yin on 2018/4/19.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit
import MZFormSheetPresentationController

class TreasureBoxViewController: BaseViewController {

    @IBOutlet weak var inputBoxView: UIView!
    @IBOutlet weak var imvOpenBox: UIImageView!
    @IBOutlet weak var imvClosedBox: UIImageView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var btnOpenBox: UIButton!
    @IBOutlet weak var btnCoin: UIButton!
    @IBOutlet weak var tfAddText: UITextField!
    @IBOutlet weak var lblStoreText: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initView() {
        
        topView.isHidden = true
        imvOpenBox.isHidden = true
        inputBoxView.isHidden = true
        btnOpenBox.isUserInteractionEnabled = false
        btnCoin.isUserInteractionEnabled = false
        
    }
    
    @IBAction func closedBoxTapped(_ sender: Any) {
        
        print("Closed box tapped")
        imvClosedBox.isHidden = true
        imvOpenBox.isHidden = false
        topView.isHidden = false
        btnOpenBox.isUserInteractionEnabled = true
        btnCoin.isUserInteractionEnabled = true
    }
    
    @IBAction func boxInsideTapped(_ sender: Any) {
        
        // get coin string from userdefault
        let defaults = UserDefaults.standard
        coinArray = defaults.stringArray(forKey: Const.K_ARRAY_COINS) ?? [String]()
        
        print("Box inside tapped")
        btnCoin.isUserInteractionEnabled = false
        
        let popVC = self.storyboard?.instantiateViewController(withIdentifier: "PopViewController") as! PopViewController
        popVC.fromHeart = false
        popVC.details = coinArray
        
        let formSheetController = MZFormSheetPresentationViewController(contentViewController: popVC)
        formSheetController.presentationController?.contentViewSize = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height-100)
        formSheetController.presentationController?.shouldDismissOnBackgroundViewTap = true
        formSheetController.contentViewControllerTransitionStyle = MZFormSheetPresentationTransitionStyle.slideAndBounceFromBottom
        //formSheetController.presentationController?.isTransparentTouchEnabled = false
        self.present(formSheetController, animated: true, completion: nil)
    }
    
    @IBAction func coinTapped(_ sender: Any) {
        print("one coin tapped")
        inputBoxView.isHidden = false
    }
    
    @IBAction func okTapped(_ sender: Any) {
        
        print("OK tapped")
        if tfAddText.text?.count != 0 {
            lblStoreText.text = tfAddText.text
            
            // save to coin array
            coinArray.append(lblStoreText.text!)
            
            let defaults = UserDefaults.standard
            defaults.set(coinArray, forKey: Const.K_ARRAY_COINS)
            
        }
        
        self.view.endEditing(true)
        tfAddText.text = ""
        inputBoxView.isHidden = true
    }
    
    
    @IBAction func cancelTapped(_ sender: Any) {
        
        print("Cancel tapped")
        
        inputBoxView.isHidden = true
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }

    

}
