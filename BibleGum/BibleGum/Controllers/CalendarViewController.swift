//
//  CalendarViewController.swift
//  BibleGum
//
//  Created by Yin on 2018/4/18.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

class CalendarViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var tblPicker: UITableView!
    @IBOutlet weak var cvCalendar: UICollectionView!
    @IBOutlet weak var calendarBgViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var everydayTopViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var imvEveryCell1: UIImageView!
    @IBOutlet weak var lblEvery1Name: UILabel!
    @IBOutlet weak var imvEveryCell2: UIImageView!
    @IBOutlet weak var lblEvery2Name: UILabel!
    
    @IBOutlet weak var redFrameView: UIView!
    @IBOutlet weak var redFrameViewXConstraint: NSLayoutConstraint!
    @IBOutlet weak var redFrameViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var redFrameViewYBottomConstraint: NSLayoutConstraint!
    
    var calendarItems = [CalendarModel]()
    var pickerImages = [String]()
    
    
    var calendarSelectedIndex: Int = 0
    var isPresented = true
    
    var everyCell1Selected: Bool = false
    var everyCell2Selected: Bool = false
    
    var calendarCellWidth: CGFloat = 0
    var calendarCellHeight: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        calendarCellWidth = (UIScreen.main.bounds.width-10.0)/8.0
        if UIScreen.main.bounds.width == 812.0 { // iPhone X issue
            calendarCellWidth = (UIScreen.main.bounds.width-10.0)/9.0
        }
        calendarCellHeight = (UIScreen.main.bounds.height-20.0-5-35-15)/4.0
        
        calendarBgViewHeightConstraint.constant = UIScreen.main.bounds.height-20-35-15
        everydayTopViewHeightConstraint.constant = (calendarBgViewHeightConstraint.constant) / 2.0 - 1.0
        
        redFrameView.layer.borderColor = UIColor.red.cgColor
        redFrameViewWidthConstraint.constant = calendarCellWidth+1.2
    }
    
    func initData() {
        
        for index in 1...Const.FACE_IMAGE_COUNT {
            
            let faceOne = "face_\(index)"
            pickerImages.append(faceOne)
        }
        
        for index in 1...Const.OBJECT_IMAGE_COUNT {
            let objectOne = "Object_\(index)"
            pickerImages.append(objectOne)
        }
        
        // get calendar images from userdefault, otherwise init with blank images
        if let _calendarImages = UserDefaults.standard.array(forKey: Const.K_CALENDAR_IMAGES) {
            calendarImages = _calendarImages as! [String]
            self.imvEveryCell1.image = UIImage(named: calendarImages[Const.CALENDAR_ITEM_COUNT])
            self.imvEveryCell2.image = UIImage(named: calendarImages[Const.CALENDAR_ITEM_COUNT+1])
        } else {
            for _ in 1...Const.CALENDAR_ITEM_COUNT + 2 {
                calendarImages.append("")
            }
        }
        
        // get calendar names from userdefault, otherwise init with blank images
        if let _calendarNames = UserDefaults.standard.array(forKey: Const.K_CALENDAR_NAMES) {
            calendarNames = _calendarNames as! [String]
            self.lblEvery1Name.text = calendarNames[Const.CALENDAR_ITEM_COUNT]
            self.lblEvery2Name.text = calendarNames[Const.CALENDAR_ITEM_COUNT+1]
        } else {
            for _ in 1...Const.CALENDAR_ITEM_COUNT + 2 {
                calendarNames.append("")
            }
        }
                
        /*
        // retrieving a value for a key
        if let data = UserDefaults.standard.data(forKey: Const.K_CALENDAR_ITEMS),
            let savedCalendars = NSKeyedUnarchiver.unarchiveObject(with: data) as? [CalendarModel] {
            calendarItems = savedCalendars
            calendarItems.forEach({print( $0.id, $0.name, $0.image!)})
        } else {
            
            // init with blank calendar objects
            print("There is an issue")
            
            calendarItems.removeAll()
            for index in 1...Const.CALENDAR_ITEM_COUNT + 2 {
                let image = UIImage()
                let calendarObj = CalendarModel(id: index, name: "", image: image)
                calendarItems.append(calendarObj)
            }
        }
         */
    }
    
    @IBAction func dismissBack(_ sender: Any) {
        
        isPresented = false
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        isPresented = false
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK:- Each Week action
    
    @IBAction func weekTapped(_ sender: Any) {
        
        let btn = sender as! UIButton
        
        UIView.animate(withDuration: Double(1.0), animations: {
            
            if btn.tag == 0 {
                self.redFrameViewXConstraint.constant = -self.redFrameViewWidthConstraint.constant
                self.redFrameViewYBottomConstraint.constant = 2 * self.calendarCellHeight
                self.redFrameView.layoutIfNeeded()
                
            } else {
                self.redFrameViewXConstraint.constant = 1+self.redFrameViewWidthConstraint.constant * CGFloat(btn.tag-1)
                self.redFrameViewYBottomConstraint.constant = 1
                self.redFrameView.layoutIfNeeded()
            }
            
        }, completion: { (finished: Bool) in
            
        })
    }
    
    
    // MARK:- Everyday cell actions
    @IBAction func everyCell1Tapped(_ sender: Any) {
        
        everyCell1Selected = true
        
    }
    
    @IBAction func everyCell1InfoTapped(_ sender: Any) {
        
        let everyCell1InfoBtn = sender as! UIButton
        everyCell1InfoBtn.tag = Const.CALENDAR_ITEM_COUNT
        infoAction(everyCell1InfoBtn)
    }
    
    @IBAction func everyCell2Tapped(_ sender: Any) {
        
        everyCell2Selected = true
        
    }
    
    @IBAction func everyCell2InfoTapped(_ sender: Any) {
        
        let everyCell2InfoBtn = sender as! UIButton
        everyCell2InfoBtn.tag = Const.CALENDAR_ITEM_COUNT + 1
        infoAction(everyCell2InfoBtn)
    }
    
    // MARK:- Info Button
    
    @objc func infoAction(_ sender: UIButton) {
        
        var title = "";
        
        if calendarNames[sender.tag].count == 0 {
            title = "Add name"
        } else {
            title = "Edit name"
        }
        
        let alert = UIAlertController(title: "Add or delete name", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let addButton = UIAlertAction(title: title, style: UIAlertActionStyle.default) { (alert) -> Void in
            print("Add name")
            
            
            let alert = UIAlertController(title: title, message: nil, preferredStyle: .alert)
            alert.addTextField { (textField) in
                textField.placeholder = "Input name here..."
            }
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
                let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
                print("Text field: \((textField?.text)!)")
                
                if sender.tag == Const.CALENDAR_ITEM_COUNT { //everyday first cell
                    self.saveCalendarName(index: sender.tag, name: (textField?.text)!)
                    self.lblEvery1Name.text = textField?.text
                    
                } else if sender.tag == Const.CALENDAR_ITEM_COUNT + 1 { //everyday second cell
                    self.saveCalendarName(index: sender.tag, name: (textField?.text)!)
                    self.lblEvery2Name.text = textField?.text
                } else {
                    self.saveCalendarName(index: sender.tag, name: (textField?.text)!)
                    self.cvCalendar.reloadData()
                }
            }))

            self.present(alert, animated: true, completion: nil)
            
        }
        
        let deleteButton = UIAlertAction(title: "Delete name", style: UIAlertActionStyle.destructive) { (alert) -> Void in

            print("delete name")
            
            self.showAlert(title: "Confirmation", message: "Are you sure want to delete?", okButtonTitle: "YES", cancelButtonTitle: "NO", okClosure: {
                
                if sender.tag == Const.CALENDAR_ITEM_COUNT { //everyday first cell
                    self.deleteCalendarName(index: sender.tag)
                    self.lblEvery1Name.text = calendarNames[sender.tag]
                } else if sender.tag == Const.CALENDAR_ITEM_COUNT + 1 { //everyday second cell
                    self.deleteCalendarName(index: sender.tag)
                    self.lblEvery2Name.text = calendarNames[sender.tag]
                } else {
                    self.deleteCalendarName(index: sender.tag)
                    self.cvCalendar.reloadData()
                }
            })
        }
        
        
        let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) { (alert) -> Void in
            print("Cancel Pressed")
        }
        
        alert.addAction(addButton)
        alert.addAction(deleteButton)
        alert.addAction(cancelButton)
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender
            popoverController.sourceRect = CGRect.init()
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK:- Save image and name
    
    func saveCalenderImage(index: Int, image: String) {
        
        calendarImages.remove(at: index)
        calendarImages.insert(image, at: index)
        
        UserDefaults.standard.set(calendarImages, forKey: Const.K_CALENDAR_IMAGES)
        
    }
    
    func saveCalendarName(index: Int, name: String) {
        
        calendarNames.remove(at: index)
        calendarNames.insert(name, at: index)
        
        UserDefaults.standard.set(calendarNames, forKey: Const.K_CALENDAR_NAMES)
    }
    
    func deleteCalendarName(index: Int) {
        
        calendarNames.remove(at: index)
        calendarNames.insert("", at: index)
        
        UserDefaults.standard.set(calendarNames, forKey: Const.K_CALENDAR_NAMES)
    }
    
    // MARK:- Picker TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return pickerImages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PickerCell") as! PickerCell
        cell.imvPhoto.image = UIImage(named: pickerImages[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      
        return calendarCellHeight + 1.2
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if everyCell1Selected {
            imvEveryCell1.image = UIImage(named: pickerImages[indexPath.row])
            everyCell1Selected = false
            calendarSelectedIndex = Const.CALENDAR_ITEM_COUNT
            
            self.saveCalenderImage(index: calendarSelectedIndex, image: pickerImages[indexPath.row])
            
        }
        
        else if everyCell2Selected {
            imvEveryCell2.image = UIImage(named: pickerImages[indexPath.row])
            everyCell2Selected = false
            calendarSelectedIndex = Const.CALENDAR_ITEM_COUNT + 1
            
            self.saveCalenderImage(index: calendarSelectedIndex, image: pickerImages[indexPath.row])
        }
            
        else {
            
            self.saveCalenderImage(index: calendarSelectedIndex, image: pickerImages[indexPath.row])
            cvCalendar.reloadData()
            
            /*
            // Save Calendar in UserDefault with object array
 
            let calendarObjc = CalendarModel(id: calendarSelectedIndex, name: "", image: pickerImages[indexPath.row])
            self.calendarItems.remove(at: calendarSelectedIndex)
            self.calendarItems.insert(calendarObjc, at: calendarSelectedIndex)
            
            
            
            let userDefaults = UserDefaults.standard
            let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: self.calendarItems)
            userDefaults.set(encodedData, forKey: Const.K_CALENDAR_ITEMS)
            userDefaults.synchronize()
            */
 
        }
    }
    
    // MARK:- Calendar CollectionView

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Const.CALENDAR_ITEM_COUNT
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CalendarCell", for: indexPath) as! CalendarCell
        cell.lblName.text = calendarNames[indexPath.row]
        cell.imvPhoto.image = UIImage(named: calendarImages[indexPath.row])
        cell.actionButton.tag = indexPath.row
        cell.actionButton.addTarget(self, action: #selector(infoAction), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        calendarSelectedIndex = indexPath.row
        
        print("calendarSelectedIndex==", calendarSelectedIndex)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //return CGSize(width: (UIScreen.main.bounds.width-10.0)/8.0, height: 100)
        return CGSize(width: calendarCellWidth, height: calendarCellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
}
