//
//  BigBubleViewController.swift
//  BibleGum
//
//  Created by Yin on 2018/4/20.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

class BigBubleViewController: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblBody: UILabel!
    var selectedIndex = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setData() {
        
        lblTitle.text = Const.BUBLE_TITLE[selectedIndex]
        lblBody.text = Const.BUBLE_BODY[selectedIndex]
    }

    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.dismiss(animated: true, completion: nil)
    }

}
