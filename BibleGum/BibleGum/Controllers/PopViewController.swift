//
//  PopViewController.swift
//  BibleGum
//
//  Created by Yin on 2018/4/20.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

class PopViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblDetails: UITableView!
    
    var details = [String]()
    var fromHeart: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tblDetails.estimatedRowHeight = 40        
        
        tblDetails.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func deleteDetail(_ sender: UIButton) {
        
        if fromHeart {
            
            self.showAlert(title: "Confirm", message: "Are you sure want to delete?", okButtonTitle: "YES", cancelButtonTitle: "NO", okClosure: {
                heartArray.remove(at: sender.tag)
                UserDefaults.standard.set(heartArray, forKey: Const.K_ARRAY_HEART)
                self.details = heartArray
                self.tblDetails.reloadData()
            })
            
        } else {
            
            self.showAlert(title: "Confirm", message: "Are you sure want to delete?", okButtonTitle: "YES", cancelButtonTitle: "NO", okClosure: {
                coinArray.remove(at: sender.tag)
                UserDefaults.standard.set(coinArray, forKey: Const.K_ARRAY_COINS)
                self.details = coinArray
                self.tblDetails.reloadData()
            })
        }
        
    }

    @IBAction func closeTapped(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    // TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return details.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCell") as! DetailCell
        
        cell.btnCenterDelete.tag = indexPath.row
        cell.btnCenterDelete.addTarget(self, action: #selector(deleteDetail), for: .touchUpInside)
        cell.lblDetail.text = details[indexPath.row]
        return cell
    }

}
