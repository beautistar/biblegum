//
//  ActsViewController.swift
//  BibleGum
//
//  Created by Yin on 2018/4/17.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

class ActsViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        lblTitle.text = ""
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTap(_ sender: Any) {
        
        let btn = sender as! UIButton
        
        switch btn.tag {
        case 0: // A
            lblTitle.text = Const.LBL_A
        case 1: // AA
            lblTitle.text = Const.LBL_AA
        case 2: // C
            lblTitle.text = Const.LBL_C
        case 3: // CC
            lblTitle.text = Const.LBL_CC
        case 4: // T
            lblTitle.text = Const.LBL_T
        case 5: // TT
            lblTitle.text = Const.LBL_TT
        case 6: // S
            lblTitle.text = Const.LBL_S
        default: // SS
            lblTitle.text = Const.LBL_SS
        }
        
    }

}
