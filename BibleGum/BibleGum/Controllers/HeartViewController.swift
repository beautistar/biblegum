//
//  HeartViewController.swift
//  BibleGum
//
//  Created by Yin on 2018/4/19.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit
import MZFormSheetPresentationController

class HeartViewController: BaseViewController {

    @IBOutlet weak var lblHeartText: UILabel!
    @IBOutlet weak var tfVerse: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        print("willappear")
    }

    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doneTapped(_ sender: Any) {
        
        if tfVerse.text?.count != 0 {
            lblHeartText.text = tfVerse.text
            
            // save to coin array
            heartArray.append(lblHeartText.text!)
            
            let defaults = UserDefaults.standard
            defaults.set(heartArray, forKey: Const.K_ARRAY_HEART)
        }
        tfVerse.text = ""
        self.view.endEditing(true)
        
    }
    
    @IBAction func heartTapped(_ sender: Any) {
        
        print("heart tapped")
        
        // get coin string from userdefault
        let defaults = UserDefaults.standard
        heartArray = defaults.stringArray(forKey: Const.K_ARRAY_HEART) ?? [String]()
        
        let popVC = self.storyboard?.instantiateViewController(withIdentifier: "PopViewController") as! PopViewController
        popVC.fromHeart = true
        popVC.details = heartArray
        
        let formSheetController = MZFormSheetPresentationViewController(contentViewController: popVC)
        formSheetController.presentationController?.contentViewSize = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height-100)
        formSheetController.presentationController?.shouldDismissOnBackgroundViewTap = true
        formSheetController.contentViewControllerTransitionStyle = MZFormSheetPresentationTransitionStyle.slideAndBounceFromBottom
        //formSheetController.presentationController?.isTransparentTouchEnabled = false
        self.present(formSheetController, animated: true, completion: nil)
        
    }
    
}
