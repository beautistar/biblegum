//
//  DetailCell.swift
//  BibleGum
//
//  Created by Yin on 2018/4/20.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

class DetailCell: UITableViewCell {

    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var btnCenterDelete: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
