//
//  CalendarCell.swift
//  BibleGum
//
//  Created by Yin on 2018/4/18.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

class CalendarCell: UICollectionViewCell {
    
    @IBOutlet weak var imvPhoto: UIImageView!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var lblName: UILabel!
}
